#!/bin/bash

sudo apt-get install terminator
sudo apt-get install i3
sudo apt-get install python3-pip 

# BASHRC	
echo "source ~/dot_files/bashrc" >> ~/.bashrc

# vim
ln -s ~/dot_files/vimrc ~/.vimrc

# TERMINATOR
mkdir -p ~/.config/terminator
ln -s ~/dot_files/terminator_config ~/.config/terminator/config

# i3
mkdir -p ~/.i3
ln -s ~/dot_files/i3_config ~/.i3/config

# git config
ln -s ~/dot_files/git_config ~/.gitconfig

#hg config
ln -s ~/dot_files/hgrc ~/.hgrc

# VIM:
# - Download vim 8:
sudo -E add-apt-repository ppa:pi-rho/dev
sudo apt-get update
sudo apt-get install vim-gnome
# - Install vundle:
mkdir -p ~/.vim/bundle
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
# - Install plugins:
vim -E +PluginInstall +qall
# - Setup YouCompleteMe
sudo apt-get install build-essential cmake
sudo apt-get install python-dev python3-dev
cd ~/.vim/bundle/YouCompleteMe
./install.py --clang-completer
cd ~/dot_files/
ln -s ~/dot_files/ycm_config ~/dot_files/.ycm_extra_conf.py

#i3
sudo apt-get install i3lock
sudo apt-get install volumeicon-alsa
sudo apt-get install help2man # dependency for light
git clone https://github.com/haikarainen/light.git
cd ./light
make
sudo make install
cd ..

#i3pystatus
pip3 install git+https://github.com/enkore/i3pystatus.git --user
# pip3 install netifaces --user
# pip3 install colour --user
#TODO: add deps?

#setup hg prompt for hg branch name as terminal prefix
#hg clone http://bitbucket.org/sjl/hg-prompt/
