#!/bin/bash

if [ -e ~/.i3/config ]; then
    echo "Please remove ~/.i3/config"
fi

if [ -e ~/.i3status.conf ]; then
    echo "Please remove ~/.i3status.conf"
fi

if [ -e ~/.gitconfig ]; then
    echo "please remove ~/.gitconfig"
fi

if [ -e ~/.hgrc ]; then
    echo "please remove ~/.hgrc"
fi

if [ -e ~/.vimrc ]; then
    echo "Please remove ~/.vimrc"
fi

if [ -e ~/.config/terminator/config ]; then
    echo "Please remove ~/.config/terminator/config"
fi

if [ ! -e ~/.bashrc ]; then
    echo "Please add ~/.bashrc"
fi
