set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'scrooloose/nerdcommenter' "https://github.com/scrooloose/nerdcommenter
Plugin 'Valloric/YouCompleteMe' "https://github.com/Valloric/YouCompleteMe
Plugin 'scrooloose/nerdtree' "https://github.com/scrooloose/nerdtree
"Plugin 'flazz/vim-colorschemes' "https://github.com/flazz/vim-colorschemes
"Plugin 'taketwo/vim-ros' "https://github.com/taketwo/vim-ros

" All of your Plugins must be added before the following line
call vundle#end()            " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" Display line numbers on the left
set number
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab

" Enable syntax highlighting
syntax on
syntax enable

"let g:solarized_visibility = "high"
"let g:solarized_contrast = "high"
"set background=dark
set t_Co=256
"let g:solarized_termcolors=256
"colorscheme ron 
"highlight Normal ctermbg=DarkBlue
highlight ErrorMsg ctermbg=Red ctermfg=White
"hi Search ctermbg=Red
hi SpellBad ctermbg=Red ctermfg=White
hi SpellCap ctermbg=Red ctermfg=White

" Enable use of the mouse for all modes
set mouse=a
set mousemodel=popup_setpos " Right-click on selection should bring up a menu

" EDITOR SETTINGS
" Use case insensitive search, except when using capital letters
set ignorecase
set smartcase

set hidden " allows making buffers hidden even with unsaved changes
set history=1000 " remember more commands and search history
set undolevels=1000 " use many levels of undo

set incsearch " turn on incremental search
autocmd BufNewFile,BufRead * setlocal formatoptions-=cro " turn off autocomment line after comment

" CLIPBOARD SETTINGS
if has('unnamedplus')
  " By default, Vim will not use the system clipboard when yanking/pasting to
  " the default register. This option makes Vim use the system default
  " clipboard.
  " Note that on X11, there are _two_ system clipboards: the "standard" one, and
  " the selection/mouse-middle-click one. Vim sees the standard one as register
  " '+' (and this option makes Vim use it by default) and the selection one as
  " '*'.
  " See :h 'clipboard' for details.
  set clipboard=unnamedplus,unnamed
else
  " Vim now also uses the selection system clipboard for default yank/paste.
  set clipboard+=unnamed
endif

set nobackup " no backup~ files.
set noswapfile " no .swp files.

" remove trailing whitespace (and keep last cursor position and search result)
fun! TrimWhitespace()
   " let l:save = winsaveview()
    %s/\s\+$//e
  "  call winrestview(l:save)
endfun
let mapleader = ","
nnoremap <Leader>rw :call TrimWhitespace()<CR>

" ******************PLUGINS*****************************
" NERDTree settings
  " automatically open when open vim tab
"autocmd vimenter * NERDTree
  " put cursor in main window instead of nerdTree window
autocmd VimEnter * wincmd p
  "change width
let g:NERDTreeWinSize = 17
  " close vim if only nerdTree window left
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let mapleader = ","
nmap <leader>ne :NERDTree<cr>
":command NE NERDTree

" YouCompleteMe settings
let mapleader=","
nnoremap <leader>gc :tab split<CR> :YcmCompleter GoToDeclaration<CR>
nnoremap <leader>gf :tab split<CR> :YcmCompleter GoToDefinition<CR>
nnoremap <leader>gt :tab split<CR> :YcmCompleter GoTo<CR>
nnoremap <leader>f :YcmCompleter FixIt<CR>
let g:ycm_confirm_extra_conf = 0 "Don't ask for confirmation every time
let g:ycm_global_ycm_extra_conf='~/dot_files/.ycm_extra_conf.py'
let g:ycm_auto_trigger = 0

" NERDcommenter settings
filetype plugin on
"[count]<leader>cc :NERDComComment

