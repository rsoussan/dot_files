#Ryan Commands
alias rgrep='grep -Iir'
alias rfind='find . -iname 2>dev/null'
alias rrsync='rsync -avzP'
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias ......="cd ../../../../.."
alias git_personal="ln -sf ~/.gitconfig_personal ~/.gitconfig"
alias git_work="ln -sf ~/.gitconfig_work ~/.gitconfig"
alias run_tex='pdflatex root.tex; bibtex root.aux; pdflatex root.tex; evince root.pdf'

# Mercurial function for fancy prompts
all_ps1() {
    hg prompt --angle-brackets "<<branch|quiet>:>" 2> /dev/null
    if [ "$?" -ne "0" ]; then
        __git_ps1
    fi
}

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\$(all_ps1)[\033[01;32m\]\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}$(all_ps1)\w\$ '
fi
unset color_prompt force_color_prompt
